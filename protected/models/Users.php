<?php

/**
 * This is the model class for table "users".
 *
 * The followings are the available columns in table 'users':
 * @property string $id
 * @property string $username
 * @property string $email
 * @property string $password
 * @property integer $type
 * @property string $pwchange_date
 * @property string $isbuyer
 * @property string $isseller
 * @property string $createdate
 *
 * The followings are the available model relations:
 * @property UserType $type0
 * @property UserProfile[] $userProfiles
 */
class Users extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'users';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, username, email, password, type', 'required'),
			array('type', 'numerical', 'integerOnly'=>true),
			array('username', 'length', 'max'=>30),
			array('email, password', 'length', 'max'=>100),
			array('isbuyer, isseller', 'length', 'max'=>1),
			array('pwchange_date, createdate', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, username, email, password, type, pwchange_date, isbuyer, isseller, createdate', 'safe', 'on'=>'search'),
		);
	}

        
        
          public function checkParams()
        {
            if(empty($this->username)||empty($this->password))
            {
                $this->addError('check_params',"Username and password cannot be empty!");
                return false;
            }
            return true;
        } 
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'type0' => array(self::BELONGS_TO, 'UserType', 'type'),
			'userProfiles' => array(self::HAS_MANY, 'UserProfile', 'userid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'email' => 'Email',
			'password' => 'Password',
			'type' => 'Type',
			'pwchange_date' => 'Pwchange Date',
			'isbuyer' => 'Isbuyer',
			'isseller' => 'Isseller',
			'createdate' => 'Createdate',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('pwchange_date',$this->pwchange_date,true);
		$criteria->compare('isbuyer',$this->isbuyer,true);
		$criteria->compare('isseller',$this->isseller,true);
		$criteria->compare('createdate',$this->createdate,true);
                
                
                if(Yii::app()->user->id['usertype']=='BUYER')
                {
                    $buyer= Users::model()->findByAttributes(array('type'=>Yii::app()->user->id['id']));
                    $criteria->addCondition("id=$buyer->id");
                }
                
                 else if(Yii::app()->user->id['usertype']=='SELLER')
                {
                    $buyer= Users::model()->findByAttributes(array('type'=>Yii::app()->user->id['id']));
                    $criteria->addCondition("id=$buyer->id");
                }
                 
                else {
                    
                    $criteria->compare('id',$this->id);
                } 

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
          public function getUser($usr, $pass)
        {
            $user = Users::model()->findByAttributes(array('username'=>$usr,'password'=>$pass));
            return $user;
        }
        
          public function checkPass($passwd)
        {
            return ($this->password == $passwd);
        } 

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Users the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
