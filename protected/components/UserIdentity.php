<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
        private $userdata = array();
        private $uinfo = array();
        /**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
            $user = Users::model()->findByAttributes(array('username'=>$this->username));
            if($user===null)
            {
                $this->errorCode=self::ERROR_USERNAME_INVALID;
            }
            else if ($user->checkPass($this->password))
            {
                $this->errorCode=self::ERROR_NONE;
                $this->setUser($user);
                //$this->setUserInfo($user->userType->user_type, $user->employee->name);
            }
            else
            {
                $this->errorCode=self::ERROR_PASSWORD_INVALID;
            }
            unset($user);
            return !$this->errorCode;             
	}
        
        private function setUserInfo($usertype, $fullname)
        {
            $this->setState('usertype', $usertype);
            $this->setState('fullname', $fullname);
        }
        
        public function getUser()
        {
            return $this->userdata;
        }

        public function setUser(CActiveRecord $user)
        {
            //TODO: find user details either from corporateclient/corporate_contact table or from 
            //jobseekers table depending on user type.
            //$this->uinfo['employeeid'] = $user->employee_id;
            $this->uinfo['usertype'] = $user->type0->type;
            $this->uinfo['id'] = $user->id;
	    $this->uinfo['username'] = $user->username;
            //$this->uinfo['fullname'] = $user->employee->name;
        }  
        
        public function getId() 
        {
            //will return the employee id
            return $this->uinfo;
        }
        
        public function getIsAdmin()
        {
            return ($this->uinfo['usertype']=='ADMIN');
        }
        
}