<?php
/* @var $this UsersController */
/* @var $model Users */


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#users-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Profile</h1>


</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'users-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'username',
		'email',
		'password',
		'type',
		'pwchange_date',
		/*
		'isbuyer',
		'isseller',
		'createdate',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
