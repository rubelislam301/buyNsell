<?php
/* @var $this UsersController */
/* @var $model Users */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'users-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
		<?php echo $form->error($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username',array('size'=>30,'maxlength'=>30)); ?>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'password'); ?>
	</div>
        
        
	<div class="row">
                <?php 
                if($model->isNewRecord)
                {
                echo $form->labelEx($model,'type'); ?>
                <?php
                $list = CHtml::listData(UserType::model()->findAll(array('order' => 'type')), 'id', 'type');
                echo $form->dropDownList($model,'type',$list);   
                ?>                
		<?php echo $form->error($model,'type'); 
                }//end - if newRecord
                ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'pwchange_date'); ?>
		<?php //echo $form->textField($model,'pwchange_date'); ?>
		<?php //echo $form->error($model,'pwchange_date'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'isbuyer'); ?>
		<?php //echo $form->textField($model,'isbuyer',array('size'=>1,'maxlength'=>1)); ?>
		<?php //echo $form->error($model,'isbuyer'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'isseller'); ?>
		<?php //echo $form->textField($model,'isseller',array('size'=>1,'maxlength'=>1)); ?>
		<?php //echo $form->error($model,'isseller'); ?>
	</div>

	<div class="row">
		<?php //echo $form->labelEx($model,'createdate'); ?>
		<?php //echo $form->textField($model,'createdate'); ?>
		<?php //echo $form->error($model,'createdate'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->