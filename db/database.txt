
-- Table: user_type

-- DROP TABLE user_type;

CREATE TABLE user_type
(
  id SERIAL PRIMARY KEY NOT NULL,
  type character varying(100),
  CONSTRAINT un_type UNIQUE (type)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_type
  OWNER TO postgres;

-- Table: users

-- DROP TABLE users;

CREATE TABLE users
(
  id SERIAL PRIMARY KEY NOT NULL,
  username character varying(30) NOT NULL,
  email character varying(100) NOT NULL,
  password character varying(100) NOT NULL,
  type integer NOT NULL,
  pwchange_date date,
  isbuyer bit(1),
  isseller bit(1),
  createdate date,
  CONSTRAINT user_type_fk_id FOREIGN KEY (type)
      REFERENCES user_type (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT unique_email UNIQUE (email),
  CONSTRAINT unique_username UNIQUE (username)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE users
  OWNER TO postgres;

-- Index: fki_user_type_fk_id

-- DROP INDEX fki_user_type_fk_id;

CREATE INDEX fki_user_type_fk_id
  ON users
  USING btree
  (type);



-- Table: user_country

-- DROP TABLE user_country;

CREATE TABLE user_country
(
  id SERIAL PRIMARY KEY NOT NULL,
  country integer,
  CONSTRAINT user_country_unique UNIQUE (country)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_country
  OWNER TO postgres;




-- Table: user_city

-- DROP TABLE user_city;

CREATE TABLE user_city
(
  id SERIAL PRIMARY KEY NOT NULL,
  city character varying(200),
  country integer,
  CONSTRAINT refer_user_country_id FOREIGN KEY (country)
      REFERENCES user_country (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT unique_city_country UNIQUE (city, country),
  CONSTRAINT user_city_unique UNIQUE (city)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_city
  OWNER TO postgres;






-- Table: user_profile

-- DROP TABLE user_profile;

CREATE TABLE user_profile
(
  id SERIAL PRIMARY KEY NOT NULL,
  full_name character varying(100),
  nickname character varying(50),
  locaton text,
  city bigint,
  country integer,
  contactno character varying(200),
  photos text,
  userid bigint NOT NULL,
  CONSTRAINT refer_city_id FOREIGN KEY (city)
      REFERENCES user_city (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT refer_country FOREIGN KEY (country)
      REFERENCES user_country (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT refer_userid FOREIGN KEY (userid)
      REFERENCES users (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE user_profile
  OWNER TO postgres;

